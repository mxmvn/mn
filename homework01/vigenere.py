def encrypt_vigenere(plaintext, keyword):
    """
    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = ''
    keyword = keyword.lower()
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    for i in range(len(plaintext)):
        n = ord(plaintext[i])
        x = alphabet.find(keyword[i % len(keyword)])
        if (96 < n < 123):
            if (96 < n + x < 123):
                ciphertext += chr(n + x)
            else:
                ciphertext += chr(n + x - 26)
        if (64 < n < 91):
            if (64 < n + x < 91):
                ciphertext += chr(n + x)
            else:
                ciphertext += chr(n + x - 26)
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ''
    keyword = keyword.lower()
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    for i in range(len(ciphertext)):
        n = ord(ciphertext[i])
        x = alphabet.find(keyword[i % len(keyword)])
        if (96 < n < 123):
            if (96 < n - x < 123):
                plaintext += chr(n - x)
            else:
                plaintext += chr(n - x + 26)
        if (64 < n < 91):
            if (64 < n - x < 91):
                plaintext += chr(n - x)
            else:
                plaintext += chr(n - x + 26)
    return plaintext
