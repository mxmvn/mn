def encrypt_caesar(plaintext):
    """
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ''
    shift = 3
    for i in range(len(plaintext)):
        n = ord(plaintext[i])
        if (64 < n < 88) or (96 < n < 120):
            n += shift
        if (87 < n < 91) or (119 < n < 123):
            n -= 23
        ciphertext += chr(n)
    return ciphertext


def decrypt_caesar(ciphertext):
    """
    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ''
    shift = 3
    for i in range(len(ciphertext)):
        n = ord(ciphertext[i])
        if (67 < n < 91) or (99 < n < 123):
            n -= shift
        if (64 < n < 68) or (96 < n < 100):
            n += 23
        plaintext += chr(n)
    return plaintext
