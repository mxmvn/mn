import pygame
from pygame.locals import *
import random


class GameOfLife:

    def __init__(self, width=640, height=480, cell_size=10, speed=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed

    def draw_grid(self):
        """ Отрисовать сетку """
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                    (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                    (0, y), (self.width, y))

    def run(self):
        """ Запустить игру """
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))

        # Создание списка клеток
        self.clist = self.cell_list()
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_grid()

            # Отрисовка списка клеток
            # Выполнение одного шага игры (обновление состояния ячеек)
            self.draw_cell_list(self.clist)
            self.update_cell_list(self.clist)
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def cell_list(self, randomize=True):
        """ Создание списка клеток.
        :param randomize: Если True, то создается список клеток, где
        каждая клетка равновероятно может быть живой (1) или мертвой (0).
        :return: Список клеток, представленный в виде матрицы
        """
        if randomize == True:
            self.clist = [[random.randint(0, 1) for i in range(self.cell_width)] for j in range(self.cell_height)]
        else:
            self.clist = [[0 for m in range(self.cell_width)] for n in range(self.height)]
        return self.clist

    def draw_cell_list(self, rects):
        """ Отображение списка клеток
        :param rects: Список клеток для отрисовки, представленный в виде матрицы
        """
        for i in range(self.cell_height):
            for j in range(self.cell_width):
                x = j * self.cell_size + 1
                y = i * self.cell_size + 1
                if rects[i][j]:
                    pygame.draw.rect(self.screen, pygame.Color('green'), (x, y, self.cell_size - 1, self.cell_size - 1))
                else:
                    pygame.draw.rect(self.screen, pygame.Color('white'), (x, y, self.cell_size - 1, self.cell_size - 1))
    
    def get_neighbours(self, cell):
        """ Вернуть список соседей для указанной ячейки
        :param cell: Позиция ячейки в сетке, задается кортежем вида (row, col)
        :return: Одномерный список ячеек, смежных к ячейке cell
        """
        neighbours = []
        row, col = cell
        neight = [(0, -1), (-1, -1), (-1, 0), (-1, 1),(0, 1), (1, 1), (1, 0), (1, -1)]
        for i, j in neight:
            if (0 <= row + i < self.cell_height) and (0 <= col + j < self.cell_width):
                neighbours.append(self.clist[row + i][col + j])
        return neighbours

    def update_cell_list(self, cell_list):
        """ Выполнить один шаг игры.
        Обновление всех ячеек происходит одновременно. Функция возвращает
        новое игровое поле.
        :param cell_list: Игровое поле, представленное в виде матрицы
        :return: Обновленное игровое поле
        """
        new_clist = [[0 for m in range(len(self.clist[0]))] for n in range(len(self.clist))]
        for i in range(len(self.clist)):
            for j in range(len(self.clist[i])):
                r = 0
                neighbours = self.get_neighbours((i,j))
                for c in range(len(neighbours)):
                    if neighbours[c] == 1:
                        r = r + 1
                if self.clist[i][j] == 1:
                    if r in {2, 3}:
                        new_clist[i][j] = 1
                elif r == 3:
                    new_clist[i][j] = 1
        self.clist = new_clist[:]
        return self.clist
        
if __name__ == '__main__':
    game = GameOfLife(320, 240, 20)
    game.run()
    